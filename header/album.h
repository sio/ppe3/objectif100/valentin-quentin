#ifndef ALBUM_H
#define ALBUM_H

#include <iostream>
#include <chrono>
#include <time.h>
#include <libpq-fe.h>

/**
 * \class Album
 * \brief Allow to retrieve the attributes from the database                         
 */
class Album{

private:
  unsigned int id;
  time_t date;
  std::string name;

public:
/**
 * Unique constructor which retrieve the value of the attributes from the database according to a track
 *
 * \param unsigned int - id_track : id of the track related to the album 
 *
 * \param PGconn * - connexion : handler for the database connexion
 */
  Album(unsigned int id_track, PGconn *);

 /**
  * \brief Destructor
  */ 
  ~Album();
  
  /**
   * \brief Get the id of the Album
   * 
   * \return id : the id of the album as an unsigned integer
   */
  unsigned int getId();

  /**
   * \brief Get the date of the album
   *
   * \return date : the date of the album as an time_t object
   */
  time_t getDate();

  /**
   * \brief Get the name of the album
   * 
   * \return name : the name of the album as a std::string
   */
  std::string getName();  
};

#endif // ALBUM_H

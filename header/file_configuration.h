#ifndef FILE_CONFIGURATION_H
#define FILE_CONFIGURATION_H

#include <fstream>
#include <iostream>
#include "json/json.h"

/**
 * \class FileConfiguration
 *
 * \brief A class made to ease the use of JSON configuration file
 */
class FileConfiguration{

public:
/**
 * \brief Unique constructor which gives a default value to the attributes
 */
  FileConfiguration();

/**
 * \brief Open and read the file to give values to the attributes  
 *
 * \param std::string - path_config : the path to the JSON config file that will be opened
 */
  void load(std::string path_config);

  /**
   * \brief Get the Database host name from the config file
   *
   * \return config_db_host : the database host name as a std::string
   */
  std::string getDbHost();

  /**
   * \brief Get the Database user name from the config file
   *
   * \return config_db_user : the database user name as a std::string
   */
  std::string getDbUser();

  /**
   * \brief Get the Database name from the config file
   *
   * \return config_db_name : the database name as a std::string
   */
  std::string getDbName();

  /**
   * \brief Get the Database schema name from the config file
   *
   * \return config_db_name : the name of the schema as a std::string
   */
  std::string getDbSchema();

  /**
   * \brief Get the Database user password from the config file
   *
   * \return config_db_password : the user's password as a std::string
   */
  std::string getDbPassword();

  /**
   * \brief Get the playlist Duration from the config file 
   *
   * \return config_duration : the palylist duration as an unsigned integer
   */
  unsigned int getDuration();

  /**
   * \brief Verify if the config file is loaded
   *
   * \return config_loaded - boolean : true if the config file has been loaded, false otherwise 
   */
  bool getConfigLoaded();

  /**
   * \brief Get the Title search criteria used to fill in the playlist from the config file 
   *
   * \return config_title : the title as a std::string
   */
  std::string getTitle();

  /**
   * \brief Get the playlist Duration from the config file 
   *
   * \return config_m3u - boolean : true if a m3u file must be created, false otherwise
   */
  bool getM3u();

  /**
   * \brief Get the playlist Duration from the config file 
   *
   * \return config_xspf - boolean : true if a xspf file must be created, false otherwise
   */
  bool getXspf();

  /**
   * \brief Get the Gender search criteria used to fill in the playlist from the config file 
   *
   * \return config_gender : the gender as a std::string
   */
  std::string getGenre();

  /**
   * \brief Get the Playlist Name search criteria used to fill in the playlist from the config file 
   *
   * \return config_name_playlist : the name of the playlist as a std::string
   */
  std::string getNamePlaylist();
  
private:
  std::string config_db_host;
  std::string config_db_user;
  std::string config_db_name;
  std::string config_db_schema;
  std::string config_db_password;
  unsigned int config_duration;
  bool config_loaded;
  std::string config_title;
  bool config_m3u;
  bool config_xspf;
  std::string config_genre;
  std::string config_name_playlist;
  
};

#endif //

#ifndef GENRE_H
#define GENRE_H

#include <string>
#include <libpq-fe.h>

/**
 * \class Genre
 * \brief Allow to retrieve the attributes from the database                         
 */
class Genre {
private:
  unsigned int id;
  std::string type;
  
public:
  /**
   * \brief Unique constructor which retrieve the value of the attributes from the database according to a track
   *
   * \param unsigned int - id_track : id of the track related to the gender
   *
   * \param  PGconn * - connexion : handler for the database connexion
   */
  Genre(unsigned int, PGconn *);

  /**
   * Destructor
   */
  ~Genre();

  /**
   * \brief Get the id of the Gender
   *
   * \return id : the id of the gender as an unsigned integer
   */
  unsigned int getId();

  /**
   * \brief Get the type of the Gender
   *
   * \return type : type of the gender as a std::string
   */
  std::string getType();
};
#endif //GENRE_H

#ifndef PLAYLIST_H
#define PLAYLIST_H

#include <iostream>
#include <vector>
#include <libpq-fe.h>
#include "track.h"
/**
 * \class Playlist
 *
 * \brief Class used for the creation of the playlist files (M3U or XSPF)
 */
class Playlist {
private:
  int id;
  std::string name;
  unsigned int duration;
  std::string export_option;
  PGconn *connection;
  

public:
  /**
   * \brief Unique Constructor which initialize the attributes with the values of the parameters
   *
   * \param integer - id : Id of the playlist (might be useful in case of a evolution of the database)
   *
   * \param std::string - name : name of the playlist file
   *
   * \param unsigned integer - duration : duration of the playlist
   *
   * \param Pgconn * - connexion : handler for the database connexion
   */
  Playlist(int id, std::string name, unsigned int duration, std::string export_option, PGconn *);

  /**
   * \brief Destructor
   */
  ~Playlist();

  /**
   * \brief Create a M3U playlist using an instance of the M3U class
   *
   * \param std::string - title : title search criteria
   *
   * \param std::string - genre : gender search criteria
   *
   * \param std::string - format : format search criteria
   *
   * \param unsigned integer - polyphony : polyphony research criteria
   *
   * \param std::string - artist : artist research criteria
   */
  void writeM3U(std::string, std::string, std::string, unsigned int, std::string);

  /**
   * \brief Create a XSPF playlist 
   *
   * \param std::string - title : title search criteria
   *
   * \param std::string - genre : gender search criteria
   *
   * \param std::string - format : format search criteria
   *
   * \param unsigned integer - polyphony : polyphony research criteria
   *
   * \param std::string - artist : artist research criteria
   */
  void writeXSPF(std::string, std::string, std::string, unsigned int, std::string);

  /**
   * \brief Get the id of the Playlist
   *
   * \return id : the id of the playlist as a integer
   */
  int getId();

  /**
   * \brief Get the duration of the Playlist
   *
   * \return duration : the duration of the playlist as an unsigned integer
   */
  unsigned int getDuration();

  /**
   * \brief Mutate the id attribute with the value of the parameter
   *
   * \param integer - id : the new id of the playlist
   */
  void setId(int id);

  /**
   * \brief Mutate the duration attribute with the value of the parameter
   *
   * \param unsigned integer - duration : the new duration of the playlist
   */
  void setDuration(unsigned int duration);
};
#endif

#include <cstdlib>
#include <fstream>
#include "../header/M3U.h"
#include "../header/log.h"

std::ofstream file;

M3U::M3U(unsigned int id, std::string name, unsigned int duration, std::string export_option, PGconn *connexion):
  id(id),
  name(name),
  duration(duration),
  export_option(export_option),
  connexion(connexion)
{
  createFile();
}

M3U::~M3U(){}

unsigned int M3U::getDuration(){
  return duration;
}

void M3U::createFile(){
  std::string file_name = name + ".m3u";
  std::string prepare_path = export_option + file_name;
  const char *path = prepare_path.c_str();
  file.open(path);
  Log logger(true, "M3U.cc");
  if(file.is_open()){
    logger.log_info("File " + file_name + " has been created");
  }
  else{
    logger.log_error("Generation of " + file_name + " has failed");
  }
  file << "#EXTM3U" << std::endl << std::endl;
}

void M3U::addTrack(unsigned int id){
  // TODO: #18
  std::string title;
  std::string artist;
  std::string path;
  unsigned int duration;
  std::string requete =  "SELECT \"morceau\".nom, duree, chemin, \"artiste\".nom FROM \"morceau\" INNER JOIN \"artiste_morceau\" ON \"morceau\".id = \"artiste_morceau\".id_morceau INNER JOIN \"artiste\" ON \"artiste_morceau\".id_artiste = \"artiste\".id WHERE \"morceau\".id = $1;";
  const char *val[] = {std::to_string(id).c_str()};
  const Oid types[] = {20};
  
  PGresult *result_req = PQexecParams(connexion,
                                     requete.c_str(),
                                     1,
                                     types,
                                     val,
                                     NULL,
                                     NULL,
                                     0);
  ExecStatusType code_retour_execution = PQresultStatus(result_req);
  if(code_retour_execution == PGRES_TUPLES_OK)
  {
    for(unsigned int compteur_tuple = 0; (int)compteur_tuple < PQntuples(result_req); ++compteur_tuple)
    {
      title = PQgetvalue(result_req, compteur_tuple, 0);
      duration = std::atoi(PQgetvalue(result_req, compteur_tuple, 1));
      path = PQgetvalue(result_req, compteur_tuple, 2);
      artist = PQgetvalue(result_req, compteur_tuple, 3);
      addToFile(title, artist, path, duration);
    }
  }

}

void M3U::addToFile(std::string title, std::string artist, std::string path, unsigned int duration){
  file << "#EXTINF:" << duration << ", " << artist << " - " << title << std::endl;
  file << path << std::endl << std::endl;
}

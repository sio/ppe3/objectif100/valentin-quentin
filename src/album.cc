#include "../header/album.h"
#include <iostream>
#include <stdlib.h>

Album::Album(unsigned int id_track, PGconn *connexion){
  std::string req = "SELECT \"album\".id, \"album\".nom FROM \"album\" INNER JOIN \"album_morceau\" on \"album_morceau\".id_album = \"album\".id INNER JOIN \"morceau\" on \"morceau\".id = \"album_morceau\".id_morceau WHERE \"morceau\".id = $1";
  const char*val[] = {std::to_string(id_track).c_str()};
  const Oid types[] = {20};

PGresult *result_req = PQexecParams(connexion,
                                     req.c_str(),
                                     1,
                                     types,
                                     val,
                                     NULL,
                                     NULL,
                                     0);
  
  
  id = atoi(PQgetvalue(result_req, 0 ,0));
  name = PQgetvalue(result_req, 0 ,1);
}

unsigned int Album::getId(){
  return id;
}

time_t Album::getDate(){
  return date;
}

std::string Album::getName(){
  return name;
}

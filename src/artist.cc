#include <iostream>
#include <stdlib.h>
#include "../header/artist.h"

Artist::Artist(unsigned int id_track, PGconn *connexion){
  std::string req = "SELECT \"artiste\".nom FROM \"artiste\" INNER JOIN \"artiste_morceau\" on \"artiste_morceau\".id_artiste = \"artiste\".id INNER JOIN \"morceau\" on \"morceau\".id = \"artiste_morceau\".id_morceauWHERE \"morceau\".id = $1";
  const char *val[] = {std::to_string(id_track).c_str()};
  const Oid types[] = {20};
  PGresult *result_req = PQexecParams(connexion,
                                     req.c_str(),
                                     1,
                                     types,
                                     val,
                                     NULL,
                                     NULL,
                                     0);

  this->id = atoi(PQgetvalue(result_req, 0, 0));
  this->name = PQgetvalue(result_req, 0,1);
}

unsigned int Artist::getId(){
  return id;
}

std::string Artist::getName(){
  return name;
}

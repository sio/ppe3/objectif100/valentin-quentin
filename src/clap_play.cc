#include <iostream>
#include <fstream>
#include "json/json.h"
#include "../header/clap_play.h"
#include "../header/file_configuration.h"

DEFINE_string(db_host, "postgresql.bts-malraux72.net", "Hostname database");
DEFINE_string(db_name, "Cours", "Name database");
DEFINE_string(db_password, "", "Password connect databse");
DEFINE_string(db_schema, "radio_libre", "Schema database");
DEFINE_uint64(duration, 5000, "Duration playlist");
DEFINE_string(title, "", "Filtering by musical title");
DEFINE_bool(xspf, false, "Type XSPF playlist");
DEFINE_bool(m3u, true, "Type M3U output playlist");
DEFINE_string(genre, "", "Filtering by musical genre");
DEFINE_string(playlist, "playlist", "Name of the playlist");
DEFINE_string(db_user, "v.tazir", "User database");
DEFINE_string(config_file, "config.json", "Configuration file path and name");
DEFINE_string(artist, "", "Filtering by musical artist");
DEFINE_string(format, "", "Filtering by musical format");
DEFINE_uint64(polyphony, 2, "Filtering by musical polyphony");
DEFINE_string(export_file, "./", "Choose your repository export");

void configure(){
  gflags::SetVersionString("1.0.0");
}

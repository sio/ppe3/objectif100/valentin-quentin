#include <stdlib.h> 

#include "../header/genre.h"

Genre::Genre(unsigned int id_track, PGconn *connexion)
{
  std::string requete = "SELECT \"genre\".id,\"genre\".type FROM \"genre\" INNER JOIN \"morceau\" ON \"genre\".id = \"morceau\".fk_genre WHERE \"morceau\".id = $1;";

  std::string id_conversion = std::to_string(id_track);
  const char *val[] = {id_conversion.c_str()};
  const Oid types[] = {20};

  PGresult *result_requete = PQexecParams(connexion,
                                         requete.c_str(),
                                         1,
                                         types,
                                         val,
                                         NULL,
                                         NULL,
                                         0);

  this->id = atoi(PQgetvalue(result_requete, 0, 0));
  this->type = PQgetvalue(result_requete, 0, 1);
}

Genre::~Genre(){}

unsigned int Genre::getId(){
  return id;
}

std::string Genre::getType(){
  return type;
}

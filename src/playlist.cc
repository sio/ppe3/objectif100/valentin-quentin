#include "../header/playlist.h"

#include <Xspf.h>
#include <XspfWriter.h>
#include <XspfProps.h>
#include <XspfXmlFormatter.h>
#include <vector>
#include <fstream>
#include "../header/M3U.h"
#include "../header/log.h"

using namespace Xspf;
using namespace Xspf::Toolbox;

Playlist::Playlist(int id, std::string name, unsigned int duration, std::string export_option, PGconn *connection) :
  id(id),
  name(name),
  duration(duration),
  export_option(export_option),
  connection(connection)
{}

Playlist::~Playlist() {}

void Playlist::writeM3U(std::string title, std::string genre, std::string format, unsigned int polyphony, std::string artist){

  M3U *m3u = new M3U(1, name, duration, export_option, connection);
  
  Track *all_tracks = new Track(1, NULL, connection);
  std::vector<unsigned int> id_tracks_filtre;
  id_tracks_filtre = all_tracks->searchTrack(getDuration(), title, genre, artist, polyphony, format);

  for(unsigned int parcours = 0; parcours < id_tracks_filtre.size(); parcours++){
    m3u->addTrack(id_tracks_filtre[parcours]);
  }
}

void Playlist::writeXSPF(std::string title, std::string genre,  std::string format, unsigned int polyphony, std::string artist)
{
  Xspf::XspfIndentFormatter formatter;
  XML_Char const *base_uri = "file:///home/radio/";
  std::time_t temps = std::time(nullptr);
  std::tm *now = std::localtime(&temps);
  Xspf::XspfDateTime *date = new Xspf::XspfDateTime(
    now->tm_year + 1900,
    now->tm_mon + 1,
    now->tm_mday,
    now->tm_hour,
    now->tm_min,
    now->tm_sec,
    0, 0);

  Xspf::XspfWriter *const playlist = Xspf::XspfWriter::makeWriter(formatter, base_uri);
  Xspf::XspfProps *props = new Xspf::XspfProps();
  props->lendTitle(name.c_str());
  props->lendDate(date);
  playlist->setProps(props);

  Track *all_tracks = new Track(2, playlist, connection);
  std::vector< unsigned int> id_tracks_filtre;
  id_tracks_filtre = all_tracks->searchTrack(getDuration(),title, genre, artist, polyphony, format);
  
  for(unsigned int parcours = 0; parcours < id_tracks_filtre.size(); parcours++){
    all_tracks->addTrack(id_tracks_filtre[parcours]);
  }

  std::string path = export_option + name;
  
  playlist->writeFile((path + ".xspf").c_str());

  std::ifstream file (name + ".xspf");
  Log logger(true, "playlist.cc");
  
  if(file.is_open()) {
    logger.log_info("File " + name + ".xspf has been created");
    file.close();
  }
  else {
    logger.log_error("Generation of " + name + ".xspf has failed");
  }    
}

unsigned int Playlist::getDuration(){
  return duration;
}

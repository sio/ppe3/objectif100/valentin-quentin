#include <stdlib.h>

#include "../header/track.h"

Track::Track(int id, Xspf::XspfWriter *const playlist,PGconn *connexion):
  id(id),
  playlist(playlist),
  connexion(connexion)
{}

Track::~Track(){}

void Track::addTrack(int id){
  std::string requete =  "SELECT \"morceau\".nom, duree, chemin FROM \"morceau\" WHERE \"morceau\".id = $1;";
  std::string _id = std::to_string(id);
  const char *val[] = {_id.c_str()};
  const Oid types[] = {20};
  PGresult *result_req = PQexecParams(connexion,
                                     requete.c_str(),
                                     1,
                                     types,
                                     val,
                                     NULL,
                                     NULL,
                                     0);

  Album *alb = new Album(id, connexion);
  Xspf::XspfTrack track;

  track.lendTitle(PQgetvalue(result_req, 0, 0));
  track.lendAlbum(alb->getName().c_str());
  track.lendAppendLocation(PQgetvalue(result_req, 0, 2));
  track.setDuration(atoi(PQgetvalue(result_req, 0, 1)));

  playlist->addTrack(track);
}

std::vector< unsigned int> Track::searchTrack(unsigned int duration_max, std::string title, std::string genre, std::string artist, unsigned int polyphony, std::string format){
  std::vector<unsigned int> tracks;
  std::string requete_filtre = "SELECT \"morceau\".id, duree FROM \"morceau\" INNER JOIN \"genre\" on \"morceau\".fk_genre = \"genre\".id INNER JOIN \"artiste_morceau\" on \"morceau\".id = \"artiste_morceau\".id_morceau INNER JOIN \"artiste\" on \"artiste_morceau\".id_artiste = \"artiste\".id INNER JOIN \"polyphonie\" on \"polyphonie\".id = \"morceau\".fk_polyphonie INNER JOIN \"format\" on \"format\".id = \"morceau\".fk_format WHERE \"morceau\".nom ~ $1 AND \"genre\".type ~ $2 AND \"artiste\".nom ~ $3 AND \"polyphonie\".nombre = $4 AND \"format\".titule ~ $5;";
  const char *val[] = {title.c_str(), genre.c_str(), artist.c_str(), std::to_string(polyphony).c_str(), format.c_str()};
  const Oid types[] = {25, 25, 25, 21, 25};
    
  // parcourir la requête
  PGresult *result_filtre = PQexecParams(connexion,
                                        requete_filtre.c_str(),
                                        5,
                                        types,
                                        val,
                                        NULL,
                                        NULL,
                                        0);

  unsigned int duration = 0;
  unsigned int nbRows = PQntuples(result_filtre);
  
  for (unsigned int i = 0; i < nbRows; i++){

    duration += atoi(PQgetvalue(result_filtre,i ,1));
    
    if (duration >= duration_max){
      break;
    }else{
      tracks.push_back(atoi(PQgetvalue(result_filtre, i,0)));
    }
  }
  
  return tracks;
}
